from django.test import TestCase

from .models import *


class CreateEventTestCase(TestCase):

    def test_create_event_model(self):
        self.assertEquals(Event.objects.count(), 0)
