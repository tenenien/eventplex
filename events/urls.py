from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^event/(?P<pk>[0-9]+)/$', EventDetailView.as_view(),
        name='event-detail'),
    url(r'^event/create/$', EventCreateView.as_view(),
        name='event-create'),
    url(r'^provider/(?P<pk>[0-9]+)/$', ProviderDetailView.as_view(),
        name='provider-detail'),
    url(r'^provider/create/$', ProviderCreateView.as_view(),
        name='provider-create'),
    url(r'^profile/(?P<pk>[0-9]+)/$', ProfileView.as_view(),
        name='profile'),
    url(r'^profile/services/(?P<pk>[0-9]+)/$', ProfileServicesView.as_view(),
        name='profile-services'),
]
