
import datetime

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, DetailView, FormView
from django.views.generic.detail import SingleObjectMixin
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect

from .models import Event, Service, ServiceProvider, RequiredService
from .forms import (EventCreateForm, EventRequiredServiceForm,
    ProviderCreateForm)


class HomeView(TemplateView):
    template_name = "home/index.html"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['page'] = 'home'
        return self.render_to_response(context)


class EventDetailView(DetailView):
    template_name = "event_detail.html"
    model = Event


class EventCreateView(FormView):
    template_name = "event_create.html"
    form_class = EventCreateForm

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates a blank version of the form.
        """
        context_data = self.get_context_data()
        context_data['services'] = Service.objects.all()
        return self.render_to_response(context_data)

    def form_valid(self, form):
        # get set service and service providers
        services = []
        for k, v in self.request.POST.items():
            if 'service' in k and 'service-provider' not in k:
                services.append(v)
        providers = []
        for k, v in self.request.POST.items():
            if 'service-provider' in k:
                providers.append(v)
        remarks = []
        for k, v in self.request.POST.items():
            if 'remarks' in k:
                remarks.append(v)
        self.object = None
        if services and providers:
            self.object = form.save()
            month = int(self.request.POST.get('month', 1))
            day = int(self.request.POST.get('day', 1))
            year = int(self.request.POST.get('year', 2016))
            self.object.event_date = datetime.datetime(year, month, day)
            self.object.save()
            for i, service_pk in enumerate(services):
                serv_obj = Service.objects.get(pk=service_pk)
                prov_obj = ServiceProvider.objects.filter(pk=providers[i])
                prov_obj = prov_obj.first() if len(prov_obj) > 0 else None
                RequiredService.objects.create(
                    event=self.object, service=serv_obj,
                    service_provider=prov_obj,
                    open_for_application=(prov_obj is None),
                    remarks=remarks[i])
        if self.object is None:
            raise ValidationError('Invalid! Might have no service or providers')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('event-detail', args=(self.object.id,))


class ProviderDetailView(DetailView):
    template_name = "provider_detail.html"
    model = ServiceProvider


class ProviderCreateView(FormView):
    template_name = "provider_create.html"
    form_class = ProviderCreateForm

    def get_success_url(self):
        return reverse('provider-detail', args=(self.object.id,))

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        context['services'] = Service.objects.all()
        return self.render_to_response(context)

    def form_valid(self, form):
        pk = self.request.POST.get('service')
        service = get_object_or_404(Service, pk=pk)
        provider = form.save(commit=False)
        provider.service = service
        provider.owner = self.request.user
        provider.save()
        return HttpResponse(status=400)


class ProfileView(TemplateView):
    template_name = "profile_view.html"


class ProfileServicesView(TemplateView):
    template_name = "profile_services.html"
