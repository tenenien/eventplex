from django.contrib import admin


from .models import *

admin.site.register(Event)
admin.site.register(Service)
admin.site.register(ServiceProvider)
admin.site.register(Offering)
admin.site.register(RequiredService)
admin.site.register(Application)
