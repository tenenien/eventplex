from django.forms import ModelForm
from django import forms
from django.contrib.admin.widgets import AdminSplitDateTime

from .models import Event, ServiceProvider, Service, RequiredService


class EventCreateForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'owner_email', 'contact_number', 'description',
                  'event_date', 'open_duration', 'budget']

    def __init__(self, *args, **kwargs):
        super(EventCreateForm, self).__init__(*args, **kwargs)
        self.fields['owner_email'].required = True
        self.fields['contact_number'].required = True
        self.fields['name'].required = True
        self.fields['description'].required = True
        self.fields['budget'].required = True


class ProviderCreateForm(forms.ModelForm):
    service = forms.ModelChoiceField(
        queryset=Service.objects.all(), empty_label=None)
    class Meta:
        model = ServiceProvider
        fields = ['name', 'owner_email', 'contact_number', 'description']


class EventRequiredServiceForm(forms.ModelForm):
    class Meta:
        model = RequiredService
        fields = ['service', 'service_provider', 'open_for_application',
                  'remarks']
