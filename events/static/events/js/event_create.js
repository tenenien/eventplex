require([
  'model', 'jquery', 'mustache', 'strftime'
],
function(Model, $, Mustache, strftime) {
  var ServiceProvider = new Model('service-provider');
  $('.service-provider-template').hide();
  var serviceProviderTemplate = $('.service-provider-template').clone();
  var selectedServices = [];
  var servicesOffered = $('.service option').length - 1;

  $('.services-field').on('change','.service', function(e) {
    var sel = $(this);
    ServiceProvider.objects.filter({service__id: sel.val()}, function(data) {
      selectedServices.push(sel.val());
      var options = [];
      $.each(data, function(k, v) {
        var o = {};
        o['name'] = v.name;
        o['id'] = v.id;
        options.push(o);
      });
      var $provider = sel.parents('.field').siblings('.field').find('.service-provider');
      $provider.empty();
      var defaultOption = $('<option></option>').attr('value', 0).text('Open for application');
      $provider.append(defaultOption);
      $.each(options, function(k, v) {
        $provider.append($('<option></option>').attr('value', v.id).text(v.name));
      });
      $provider.focus();
    });
  });

  $('.services-field').on('click','.add.square.icon', function() {
    var parentField = $(this).parents('.three.fields');
    if (parentField.find('.service').val() === "" ||
        parentField.find('.service-provider').val() === "") {
      return;
    }
    $('.service .add.square.icon').remove();
    var template = serviceProviderTemplate.html();
    $('.services-field').append(template);
    for (var i = 0; i < selectedServices.length; i++) {
      $('.services-field .service').last().find('option[value="'+ selectedServices[i] +'"]').remove();
    }
    $('.services-field .service').last().attr('name', 'service'+selectedServices.length);
    $('.services-field .service-provider').last().attr('name', 'service-provider'+selectedServices.length);
    $('.services-field .remarks').last().attr('name', 'remarks'+selectedServices.length);
    $(this).remove();
    if (servicesOffered === selectedServices.length - 2) {
      $('.add.square.icon').remove();
    }
  });

});
