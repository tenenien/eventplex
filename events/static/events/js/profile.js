require([
  'model', 'jquery', 'mustache', 'strftime'
],
function(Model, $, Mustache, strftime) {
  var Event = new Model('event');

  Event.objects.filter({'owner__pk': user, 'owner__email': user_email },
    function(data) {
    for (var i = 0; i < data.length; i++) {
      var event = data[i];
      var template = $('#event-record-template').html();
      var render = Mustache.render(template, event);
      $('tbody').append(render);
    }
  });
});
