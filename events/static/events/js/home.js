require([
  'model', 'jquery', 'mustache', 'strftime', 'semantic',
],
function(Model, $, Mustache, strftime, cookies) {
  var Event = new Model('event');
  var Service = new Model('service');
  var ServiceProvider = new Model('service-provider');
  var active = $('.active');

  fetchEvents();

  $('.masthead a.item').on('click', function(e) {
    e.preventDefault();
    if ($(this).data('menu')) {
      var item = $(this).data('menu');
      $('.masthead a').removeClass('active');
      $(this).addClass('active');
      $('.grid-list').addClass('hidden');
      $('.grid-list').empty();
      if (item === 'event') {
        $('.entry-list').removeClass('hidden');
        $('.entry-list .card').fadeIn('slow');
        fetchEvents();
      } else if (item === 'catalog') {
        $('.services-list').removeClass('hidden');
        $('.services-list .card').fadeIn('slow');
        fetchCatalogs();
      }
    }
  });

  function fetchEvents() {
    Event.objects.filter({}, function(data) {
      for (var i = 0; i < data.length; i++) {
        var event = data[i];
        var template = $('#event-widget').html();
        var render = Mustache.render(template, event);
        $('.entry-list').append(render);
      }
    });
  }

  function fetchCatalogs() {
    ServiceProvider.objects.filter({}, function(data) {
      var services = {};
      // save services
      for (var i = 0; i < data.length; i++) {
        var provider = data[i];
        services[provider.service.id] = provider.service;
        services[provider.service.id].providers = [];
      }
      for (var i = 0; i < data.length; i++) {
        var provider = data[i];
        services[provider.service.id].providers.push(provider);
      }
      for (var key in services) {
        var service = services[key];
        var template = $('#service-widget').html();
        var render = Mustache.render(template, service);
        $('.services-list').append(render);
      }
    });
  }

    $('.login-required').on('click', function(e) {
        if (!window.authenticated) {
          e.preventDefault();
          $('.ui.basic.modal').modal('show');
        }
    });
});
