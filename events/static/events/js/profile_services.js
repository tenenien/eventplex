require([
  'model', 'jquery', 'mustache', 'strftime'
],
function(Model, $, Mustache, strftime) {
  var ServiceProvider = new Model('service-provider');

  ServiceProvider.objects.filter({'owner__pk': user, 'owner__email': user_email },
    function(data) {
    for (var i = 0; i < data.length; i++) {
      var service = data[i];
      var template = $('#service-record-template').html();
      var render = Mustache.render(template, service);
      $('tbody').append(render);
    }
  });
});
