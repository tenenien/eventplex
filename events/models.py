from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models

from django.utils import timezone


class Event(models.Model):
    owner = models.ForeignKey(User, related_name="events",
                              null=True, blank=True)
    name = models.CharField(max_length=140, null=True, blank=True)
    owner_email = models.EmailField(max_length=254, null=True, blank=True)
    contact_number = models.CharField(max_length=140)
    description = models.TextField(null=True, blank=True)
    when = models.DateTimeField(auto_now_add=True)
    event_date = models.DateTimeField(null=True, blank=True)
    open_duration = models.DurationField(null=True, blank=True)
    budget = models.FloatField(null=True, blank=True)

    @property
    def status(self):
        total_services = self.required_services.all()
        complied_services = total_services.filter(
            service_provider__isnull=False).count()
        if complied_services == total_services.count():
            now = timezone.now()
            if now < self.event_date:
                status = 'UPCOMING'
            elif now == self.event_date:
                status = 'ONGOING'
            else:
                status = 'DONE'
        else:
            status = 'INCOMPLETE'

        return status

    @property
    def url(self):
        return reverse('event-detail', args=(self.pk, ))

    def __unicode__(self):
        return "{0}'s {1}".format(self.owner, self.name)


class Service(models.Model):
    name = models.CharField(max_length=140, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.name


class ServiceProvider(models.Model):
    owner = models.ForeignKey(User, related_name="providers",
                              null=True, blank=True)
    name = models.CharField(max_length=140, null=True, blank=True)
    owner_email = models.EmailField(max_length=254, null=True, blank=True)
    contact_number = models.CharField(max_length=140, null=True, blank=True)
    service = models.ForeignKey(Service, related_name="providers")
    description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.name

    @property
    def url(self):
        return reverse('provider-detail', args=(self.pk, ))


class Offering(models.Model):
    service_provider = models.ForeignKey(ServiceProvider,
                                         related_name="offerings")
    name = models.CharField(max_length=140, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    price = models.FloatField(null=True, blank=True)

    def __unicode__(self):
        return "{0}'s {1}".format(self.service_provider, self.name)


class RequiredService(models.Model):
    event = models.ForeignKey(Event, related_name="required_services")
    service = models.ForeignKey(Service,
                                related_name="required_services")
    service_provider = models.ForeignKey(ServiceProvider,
                                         related_name="required_services",
                                         null=True, blank=True)
    open_for_application = models.BooleanField()
    remarks = models.TextField()

    def __unicode__(self):
        return "{0} of {1}".format(self.service, self.event)


class Application(models.Model):
    required_service = models.ForeignKey(RequiredService,
                                         related_name="applications")
    service_provider = models.ForeignKey(ServiceProvider,
                                         related_name="applications")
    remarks = models.TextField()
    accepted = models.BooleanField()

    def __unicode__(self):
        return "{0}'s application for {1}".format(
            self.service_provider, self.required_service)
