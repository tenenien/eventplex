from tastypie import fields
from tastypie.api import Api
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.resources import ModelResource

from .models import (Event, Service, ServiceProvider, Offering,
    RequiredService, Application)



events_api = Api(api_name='api')


class EventResource(ModelResource):
    status = fields.CharField(attribute='status', readonly=True)
    url = fields.CharField(attribute='url', readonly=True)

    class Meta:
        queryset = Event.objects.all()
        resource_name = 'event'
        filtering = {
            'status': ALL,
        }
        ordering = ['-when']


class ServiceResource(ModelResource):
    class Meta:
        queryset = Service.objects.all()
        resource_name = 'service'
        filtering = {
            'id': ALL,
        }


class ServiceProviderResource(ModelResource):
    service = fields.ForeignKey(ServiceResource, 'service', full=True)
    url = fields.CharField(attribute='url', readonly=True)

    class Meta:
        queryset = ServiceProvider.objects.all()
        resource_name = 'service-provider'
        filtering = {
            'service': ALL_WITH_RELATIONS,
        }


class OfferingResource(ModelResource):
    class Meta:
        queryset = Offering.objects.all()
        resource_name = 'offering'


class RequiredServiceResource(ModelResource):
    class Meta:
        queryset = RequiredService.objects.all()
        resource_name = 'required-service'


class ApplicationResource(ModelResource):
    class Meta:
        queryset = Application.objects.all()
        resource_name = 'application'


events_api.register(EventResource())
events_api.register(ServiceResource())
events_api.register(ServiceProviderResource())
events_api.register(OfferingResource())
events_api.register(RequiredServiceResource())
events_api.register(ApplicationResource())
