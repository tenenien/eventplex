(function(config) {
    if (window.hasOwnProperty('require')) {
        require.config(config);
    } else {
        window.require = config;
    }
})({
    baseUrl: '/static/',
    paths: {
        jquery: 'core/js/jquery/jquery-1.11.3.min',
        stapes: 'core/js/stapes/stapes-0.8.1-min',
        text: 'core/js/text/text',
        mustache: 'core/js/mustache/mustache',
        model: 'core/js/model/model',
        strftime: 'core/js/strftime/strftime',
        semantic: 'core/js/semantic/semantic.min',
    },
    shim: {
        semantic: {
            deps: ['jquery'],
        },
        jquery: {
            exports: '$'
        }
    }
});
